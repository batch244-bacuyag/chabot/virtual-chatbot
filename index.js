const messagesContent = $('.msgs-content');
const messagesInput = $('.msg-input');
const messagesSubmit = $('.msg-submit');
const avatarImage = 'bx bx-bot';
const apiKey = "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";


function getCurrentTime() {
  const now = new Date();
  const hour = now.getHours().toString().padStart(2, '0');
  const minute = now.getMinutes().toString().padStart(2, '0');
  return `${hour}:${minute}`;
}
// Function to add the initial message
function addInitialMessage() {
  messagesContent.append(`
  <div class="message bot-message">
    <div class="avatar-container">
      <i class='${avatarImage}'></i>
    </div>
    <div class="bot-text-container">
      <span> Hi! I am your Virtual Chatbot. How can I help you?</span>
    </div>
  </div>
  <div class="bot-timestamp">${getCurrentTime()}</div>
  `);
}

// Call the function to add the initial message when the page loads
$(document).ready(function() {
  addInitialMessage();
});

// Event listener for sending a message
messagesSubmit.on('click', async () => {
  const message = messagesInput.val();
  messagesInput.val('');

  messagesContent.append(`
    <div class="message-container">
      <div class="message user-message">
        <div class="user-text-container">
          <span>${message}</span>
        </div>
      </div>
      <div class="user-timestamp">${getCurrentTime()}</div>
    </div>
  `);
  messagesContent.scrollTop(messagesContent.prop('scrollHeight'));
  
  const response = await axios.post('https://api.openai.com/v1/completions', {
    prompt: message,
    model: 'text-davinci-003',
    temperature: 0,
    max_tokens: 100,
    top_p: 1,
    frequency_penalty: 0,
    presence_penalty: 0
  }, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${apiKey}`
    }
  });


  const chatbotResponse = response.data.choices[0].text;

  messagesContent.append(`
    <div class="message bot-message">
      <div class="avatar-container">
        <i class='${avatarImage}'></i>
      </div>
      <div class="bot-text-container">
        <span>${chatbotResponse}</span>
      </div>
    </div>
    <div class="bot-timestamp">${getCurrentTime()}</div>
  `);

  // Scroll to the bottom of the chat window after adding messages
  messagesContent.scrollTop(messagesContent.prop('scrollHeight'));


});
